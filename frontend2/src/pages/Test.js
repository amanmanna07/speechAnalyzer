import axios from 'axios'
import React from 'react'

export default function Test() {
    let handleOnClick = async () => {
        const response = await axios.get('http://localhost:5000/result/fetchallresults', {
            headers: {
                // 'Content-Type': 'multipart/form-data',
                // 'Content-Type': 'application/json'
                'auth-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNjNkYjk0NzFkZjU2MGIwYzQ0MDUwNzFiIn0sImlhdCI6MTY3NTM1NDM4Mn0.0k7HJmMlD1oyUpxDj8fnWgPvTH2UyCV8CkwjOlPzdog'
            }
        });
        console.log(response.data[0].user);
    }
    return (
        <button onClick={handleOnClick}>
            Click me
        </button>
    )
}
