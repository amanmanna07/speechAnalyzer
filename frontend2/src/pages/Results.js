import React, { useState, useEffect } from 'react'
import axios from 'axios'
import ResultCard from '../components/ResultCard';
import { useNavigate } from 'react-router';

export default function Results() {
  const [results, setResults] = useState([]);
  let navigate=useNavigate();
  useEffect(() => {
    let fetchCall = async () => {
      const resultResponse = await axios.get(`http://localhost:5000/result/fetchallresults`, {
        headers: {
          'auth-token': localStorage.getItem('token')
        }
      });
      setResults(resultResponse.data);
    }
    if(localStorage.getItem('token')){
      fetchCall();
    }
    else{
      navigate('/');
    }
  }, [])

  return (
    <div className='container'>
      {results && results.map((item) => {
        return <div style={{'width':'100%'}}>
          <ResultCard key={item._id} item={item} />
        </div>
        
      })}
    </div>
  )
}
