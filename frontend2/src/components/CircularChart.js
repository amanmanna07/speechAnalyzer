import React from 'react'
import { Doughnut } from 'react-chartjs-2'
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';

ChartJS.register(ArcElement, Tooltip, Legend);

export default function CircularChart(props) {
  const data = {
    datasets: [
      {
        backgroundColor: ['rgb(255, 99, 132)', 'rgb(236, 222, 204)'],
        data: [props.score, (100 - props.score)],
        borderWidth: 0
      }
    ],

  }
  const options = {
    plugins: {
      legend: {
        display: false
      }
    }
  }


  return (
    <div style={{ 'width': '80px' }} >
      <Doughnut data={data} config={options} />
    </div>
  )
}
