import React from 'react'
import CircularChart from './CircularChart'

export default function Overview(props) {
    return (
        <div className='d-flex flex-column ' >
            {/* Set this to a chart */}
            <div className='d-flex flex-row my-3 justify-content-between'>
                <div className='d-flex flex-row'>
                    <CircularChart score={props.resultObj.overallScore}></CircularChart>
                    <div className='d-flex align-items-center mx-4'><h3>Overall Score : {props.resultObj.overallScore}%</h3></div>
                </div>
                <div className='d-flex flex-column ms-2'>
                    <h5 className='mb-2 text-center'>Your Audio</h5>
                    <audio src={props.audioURL} controls></audio>
                </div>
            </div>
            <div className='d-flex flex-row my-3 justify-content-between'>
                <div className='d-flex flex-column me-2'>
                    <h5>What you said?</h5>
                    <p>{props.resultObj.transcribe}</p>
                </div>

            </div>
            <div className='my-1'> <span style={{ 'fontWeight': '650' }}> Total words : </span>{props.resultObj.wordCount}</div>
            <div className='my-1'> <span style={{ 'fontWeight': '650' }}> Most Used Words : </span>{props.resultObj.mostUsedWords.join(', ')}</div>
            <div className='my-1'> <span style={{ 'fontWeight': '650' }}> Least Used Words : </span>{props.resultObj.leastUsedWords.join(', ')}</div>
        </div>
    )
}
