import React from 'react'
import BarChart from './BarChart'

export default function Vocabulary(props) {
    return (
        <>
            <div className='d-flex flex-column my-2'>
                <div className='d-flex flex-column my-2'>
                    <div style={{'fontWeight':'600'}} >Advance words used</div>
                    <div> {props.resultObj.difficultyArray[3].length!=0? props.resultObj.difficultyArray[3].join(', '):<span style={{'color':'red'}}>No words to display</span>}</div>
                </div>
                <div className='my-2'>
                    <BarChart resultObj={props.resultObj}></BarChart>
                </div>
            </div>
        </>

    )
}
