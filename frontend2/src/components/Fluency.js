import React from 'react'

export default function Fluency(props) {
  return (
    <div className='d-flex flex-column my-2'>
      <div style={{'fontSize':'18px'}}><span style={{'fontWeight':'650'}}>Speed : </span>{props.resultObj.fluency} WPM</div>
      <div className='my-2'> <span style={{'fontWeight':'600'}}>Tip : </span> Keeping your pace in between 120 wpm to 180 wpm will make your audience engaged.</div>
    </div>
  )
}
