import React from 'react'
import micoff from './pictures/miciconmuted.png'

export default function MicOff() {
  return (
    <div className='text-center mb-3 mt-2' >
        <img src={micoff} alt="Microphone Off" height='60px'></img>
    </div>
  )
}
