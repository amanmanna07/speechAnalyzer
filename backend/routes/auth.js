const express = require('express');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const fetchuser = require('../middleware/fetchuser');
require('dotenv').config({ path: '../.env' });
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const router = express.Router();

router.use(jsonParser);

//Route 1- SignUp user
// console.log(process.env.JWT_SECRET);

router.post('/signup', async (req, res) => {

    try {
        let success = false;
        const { email, password, name, phoneNumber, age } = req.body;
        // Check if the user with the provided email already exist
        let user = await User.findOne({ email });
        if (user) {
            return res.status(401).json({ success, error: 'Sorry a user with this email already exists' });
        }
        // Check if the user with the provided phone number already exist
        user = await User.findOne({ phoneNumber });
        if (user) {
            return res.status(400).json({ success, error: 'Sorry a user with this phone number already exists' });
        }
        const salt = await bcrypt.genSalt(10);
        const securedPass = await bcrypt.hash(`${password}${process.env.PEPPER}`, salt); // Hashing the password
        user = await User.create({
            name, email, phoneNumber, password: securedPass, age
        })
        const data = {
            user: {
                id: user.id,
            }
        }
        success = true;
        console.log(data);
        const authToken = jwt.sign(data, `${process.env.JWT_SECRET}`);
        res.json({ success, authToken });
    }
    catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})

// Route 2 - Login the user

router.post('/login', async (req, res) => {
    try {
        let success = false;
        const { email, password } = req.body;
        // Check if the user with the provided email already exist
        let user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({ success, error: 'Please enter the proper cedentials' });
        }
        const passwordCompare = await bcrypt.compare(`${password}${process.env.PEPPER}`, user.password);
        if (!passwordCompare) {
            return res.status(400).json({ success, error: 'Please enter the proper cedentials' });
        }
        const data = {
            user: {
                id: user.id,
            }
        }
        success = true;
        const authToken = jwt.sign(data, `${process.env.JWT_SECRET}`);
        res.json({ success, authToken });
    }
    catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})


// Route 3 : Get the details of the logged in user

router.post('/getuser', fetchuser, async (req, res) => {
    try {
        const userId = req.user.id;
        const user = await User.findById(userId).select('-password');
        res.send(user);
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})

module.exports = router;