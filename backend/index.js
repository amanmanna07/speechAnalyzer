const express=require('express');
const connectToMongo=require('./db/db');
const authRoute=require('./routes/auth');
const resultRoute=require('./routes/result');
const speechRoute=require('./routes/speech');

connectToMongo();

const app=express();
const port=5000
const cors=require('cors');

app.use(cors());

//Using Routes
app.use('/user',authRoute);
app.use('/speech',speechRoute);
app.use('/result',resultRoute);


app.listen(port,()=>{
    console.log(`Speech Analyzer listening on the port ${port}`);
})