const mongoose = require('mongoose');

const speechSchema = new mongoose.Schema({
    audio: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },
    user:{
        type:  mongoose.Schema.Types.ObjectId ,
        ref: 'user'
    },
    audioURL:{
        type:String,
        required:true
    },
    date: {
        type: Date,
        default: Date.now()
    }
})

const Speech = mongoose.model('speech', speechSchema);

module.exports = Speech;